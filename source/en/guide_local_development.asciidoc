[[guide_local_development]]
=== Goals

This guide assists in creating and installing a new Drupal application on a local machine. Installing Drupal using the following instructions will give you a starting point for a website that can be deployed to a production environment. Developers are the intended audience for this guide.

[TIP]
If you are evaluating Drupal's capabilities and codebase, and are not yet starting development, you can <<guide_evaluator, Create a disposable Drupal demo application>>.

[[prereqs]]
=== Prerequisites/assumptions

* you can perform basic operations using the commandline. See link:https://drupalize.me/videos/moving-around-command-line?p=1149[Moving Around the Command Line video tutorial].
* you have system privileges to download and install new software.
* your system has the following installed
  * php
  * curl
  * composer
  * Docker

[[ddev]]
=== Proposed solution: use DDEV for local development

This guide provides instructions for meeting link:https://www.drupal.org/docs/8/system-requirements[Drupal's system requirements] using a free, cross-platform local development solution, link:https://ddev.readthedocs.io/en/latest/[DDEV]. Many viable link:https://www.drupal.org/docs/develop/local-server-setup[local development environment solutions] exist. Note: DDEV requires Docker.

=== Download and install DDEV

Execute the following command to install DDEV:
....
curl -L https://raw.githubusercontent.com/drud/ddev/master/scripts/install_ddev.sh | bash
....

For more information, view the link:https://ddev.readthedocs.io/en/latest/#installation[DDEV installation documentation].

[[create_application]]
=== Fetch Drupal and its dependencies with composer

Use link:https://getcomposer.org/download/[Composer] to assemble a new Drupal codebase. Composer will also help you maintain the modules, themes, profiles, libraries, etc. as updates are released and your requirements evolve. It is best practice to link:https://www.drupal.org/docs/develop/using-composer/using-composer-to-manage-drupal-site-dependencies[ensure that your entire Drupal application is managed with Composer].

In the following commands, replace `my-site-name` with the machine-friendly name of your new application (use only letters, numbers and dashes in the name). Also replace path/to/dev/directory with the path to the directory in which you would like to place your new application.

These commands will name your application, create a new directory at your selected location, populate that new directory with a Drupal codebase and navigate to it:

....
# Replace my-site-name
export SITE_NAME=my-site-name
# Replace path/to/dev/directory
cd path/to/dev/directory
composer create-project drupal-composer/drupal-project:8.x-dev --stability dev --no-interaction $SITE_NAME
cd $SITE_NAME
....

[[config_local_serve_app]]
=== Configure and start your local development server

The following commands create a new database and configure the web server to serve your Drupal application at a particular URL:

....
ddev config --docroot web --project-name $SITE_NAME --project-type drupal8
....

The generated configuration is stored in a new `.ddev` subdirectory.

Next, ensure Docker is running. Once it is, use the following command to start the DDEV containers:

....
ddev start
....

You now have a web server and a database server configured and running in their own containers.

[[install_drupal]]
=== Install Drupal

Next, use an installation profile to populate the new database of your Drupal application. The following command opens your new Drupal site and starts the in-browser configuration wizard:

....
open http://$SITE_NAME.ddev.local
....

Alternatively, if you would like to install Drupal using the commandline, then open your new Drupal site, use the following commands:

....
ddev exec drush site-install
open http://$SITE_NAME.ddev.local
....

(The above command uses Drush, a popular command-line utility for Drupal that comes installed with DDEV.)

You now have a fully functional development site running on your local machine. A set of .gitignore files and local settings files were created during the installation to keep the configuration for your local setup, such as the database credentials, out of your repository.

[[next_steps]]
=== Next steps

What now?

Learn how you can begin to extend and customize your new Drupal application. Visit the link:https://www.drupal.org/docs/user_guide/en/index.html[Drupal 8 User Guide] and read the following chapters:

* link:https://www.drupal.org/docs/user_guide/en/config-chapter.html[Chapter 4. Basic Site Configuration]
* link:https://www.drupal.org/docs/user_guide/en/content-chapter.html[Chapter 5. Basic Page Management]
* link:https://www.drupal.org/docs/user_guide/en/content-structure-chapter.html[Chapter 6. Setting Up Content Structure]

[[appendix]]
=== Appendix

We chose to use DDEV for this local development guide because it met the following criteria:

1. Must be free and open source, without tying users into a proprietary product or service.
1. Must be well maintained, with long term support.
1. Must follow Drupal best practices.
1. Must be compatible with MacOS, Windows, and Linux.
1. Must be as simple as possible
  * Fewest pre-requisites
  * Fewest manual steps

To suggest a different local development solution, please create an issue in the link:https://www.drupal.org/project/issues/official_docs?categories=All[Official Docs issue queue].
